# README #

Smart office assistant **Joanna 2.0** helps employees query the company's knowledge base and various IT systems.

##Features of Joanna 2.0:

1. Queries the company's knowledge base about the employees, their role and responsibilities.
2. Checks room bookings in a given location and allows you to make a new reservation.
3. Retrieves and displays processes in place within the organisation.
4. Allows administrators to browse the list of unanswered questions and add new entries to the knowledge base.