﻿open ChatbotContestGenerator.PeopleGenerator
open System.IO
open System

/// Create a file name based on a name and an extension
let getFilename baseName extension withTimestamp =

    let formattedExtension =
        match extension:string with
        | ext when ext.StartsWith(".") -> sprintf "%s" (ext.Substring(1, ext.Length-1))
        | (ext) -> sprintf "%s" ext
        
    match withTimestamp with
    | true ->
        let timestamp = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")
        sprintf "%s%s.%s" baseName timestamp formattedExtension
    | _ -> sprintf "%s.%s" baseName formattedExtension

let private saveToFile (filename:string) (content:string) = async {
    use writer = new StreamWriter(new FileStream(filename, FileMode.CreateNew))
    do! writer.WriteAsync(content) |> Async.AwaitTask
    printfn "new file '%s' created!" filename
}

[<EntryPoint>]
let main argv =

    // The URL of the data generator
    let url = "https://www.mockaroo.com/f429de70/download?count={count}&key=df449e20"
    
    printfn "\n*****************************************************************************"
    printfn "\nWelcome to the random people generator! Mockaroo generator URL:"
    printfn "\n%s" url
    printfn "\n*****************************************************************************"

    printfn "\n\nHow many entries do you want to generate (range 1 - 5000)?"
    let numberToGenerate = int (System.Console.ReadLine())
    printfn "\n\nGenerating a new random file with %i people..." numberToGenerate

    GeneratePeopleJson url numberToGenerate
    |> saveToFile (getFilename "./people_" "json" true)
    |> Async.RunSynchronously

    printfn "\nPress a key to exit..."
    System.Console.ReadKey() |> ignore
    0