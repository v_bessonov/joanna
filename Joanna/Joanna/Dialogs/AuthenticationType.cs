﻿namespace Joanna.Dialogs
{
    public enum AuthenticationType
    {
        AzureActiveDirectory,
        Manual
    }
}