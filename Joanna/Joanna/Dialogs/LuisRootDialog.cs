﻿using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Bot.Connector;
using System.Threading;
using Joanna.Helpers;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Joanna.Models;
using Microsoft.Bot.Builder.Internals.Fibers;
using QnAMakerDialog;
using Joanna.Services;

namespace Joanna.Dialogs
{
    [LuisModel("fake", "fake")]
    [Serializable]
    public class LuisRootDialog : LuisDialog<object>
    {
        private readonly IEntityToType entityToType;
        private readonly IProcessService processService;
        private readonly IQuestionDbService queryService;
        private readonly QnAMakerDialog<Guid> qnadialog;
        private readonly IEmployeeService employeeService;

        public LuisRootDialog(
            IEntityToType entityToType, QnAMakerDialog<Guid> qnadialog, ILuisService luisService,
            IProcessService processService, IQuestionDbService queryService, IEmployeeService employeeService) : base(luisService)
        {
            SetField.NotNull(out this.entityToType, nameof(entityToType), entityToType);
            SetField.NotNull(out this.processService, nameof(processService), processService);
            SetField.NotNull(out this.queryService, nameof(queryService), queryService);
            SetField.NotNull(out this.qnadialog, nameof(qnadialog), qnadialog);
            SetField.NotNull(out this.employeeService, nameof(employeeService), employeeService);
        }

        protected override async Task DispatchToIntentHandler(IDialogContext context,
                                                            IAwaitable<IMessageActivity> item,
                                                            IntentRecommendation bestInent,
                                                            LuisResult result)
        {
            await Wrapping(context, async () => await base.DispatchToIntentHandler(context, item, bestInent, result));
        }

        private async Task Wrapping(IDialogContext context,
            Func<Task> action)
        {
            try
            {
                await action();
            }
            catch (Exception ex)
            {
                await context.PostAsync($"{ex.Message}");
            }
            finally
            {
                context.Done(string.Empty);
            }
        }

        [LuisIntent("")]
        [LuisIntent("None")]
        public async Task None(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult result)
        {
            var messageToForward = await message;

            await context.Forward(qnadialog, AfterQnADialog, messageToForward, CancellationToken.None);
        }

        private async Task AfterQnADialog(IDialogContext context, IAwaitable<Guid> result)
        {
            var ticketNumber = await result;
        }

        // Hello
        [LuisIntent("Smalltalks.Hello")]
        public async Task Hello(IDialogContext context, LuisResult result)
        {
            var user = await context.GetCurrentUser(employeeService);
            await context.PostAsync($"Hello {user.FirstName}, how may I be of assistance today?");
        }

        // Help
        [LuisIntent("Smalltalks.Help")]
        public async Task Help(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("I usually do the following:\n" +
                "1. Query the company's knowledge base about the employees, their role and responsibilities.\n" +
                "2. Retrieve and display processes which are in place within the organization.\n" +
                "3. Allow administrators to browse the list of unanswered questions and add new entries to the knowledge base.");
        }

        private Range<DateTime>? GetPeriod(LuisResult result)
        {
            var nowForUser = DateTime.Now; //TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, user.Location.Zone);
            // hope this code is working ok. it should return period from luis result
            IEnumerable<Range<DateTime>> ranges;
            Range<DateTime>? period = null;
            if (entityToType.TryMapToDateRanges(nowForUser, result.Entities, out ranges))
            {
                using (var enumerator = ranges.GetEnumerator())
                {
                    if (enumerator.MoveNext())
                    {
                        period = enumerator.Current;
                    }
                }
            }

            return period;
        }

        #region Dialog 1 Querying hierarchy & responsibilities

        private static EntityRecommendation GetDefaultLocationEntity(LuisResult result, Employee user)
        {
            EntityRecommendation locationEntity;
            if (!result.TryFindEntity(Constants.Entity_Location, out locationEntity))
            {
                locationEntity = new EntityRecommendation(type: Constants.Entity_Location) { Entity = user.Location };
            }

            return locationEntity;
        }

        [LuisIntent("PeopleAndRoles.WhoIsResponsibleFor")]
        public async Task WhoIsResponsibleFor(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult luisResult)
        {
            var user = await context.GetCurrentUser(employeeService);
            EntityRecommendation responsibilityEntity;
            if (luisResult.TryFindEntity(Constants.Entity_Responsibility, out responsibilityEntity))
            {
                var location = luisResult.TryGetLocation();
                if (location.IsEmpty()) // no location provided
                {
                    var employees = await employeeService.GetByResponsibility(responsibilityEntity.Entity);

                    await employees.ProcessAsync(
                        async () => // no one found
                        {
                            await context.PostAsync($"Since you didn't provide a specific location, I looked globally. Unfortunately it seems that no one is responsible for {responsibilityEntity.Entity}!");
                        },
                        async employee => // only one person found
                        {
                            await context.PostAsync($"It looks like I found one person who handles {responsibilityEntity.Entity} globally. It's\n\n{employee.ToString()}");
                        },
                        async employeesList => // several employees found
                        {
                            var listOfPersons = String.Join("\n\n", employeesList.Select(e => e.ToString()));
                            await context.PostAsync($"I found a bunch of folks who can help you with {responsibilityEntity.Entity} globally. Here's the full list:\n\n{listOfPersons}");
                        }
                    );
                }
                else // custom location provided
                {
                    var employees = await employeeService.GetByResponsibility(responsibilityEntity.Entity, location);

                    await employees.ProcessAsync(
                        async () => // no one found
                        {
                            await context.PostAsync($"I'm afraid no one is in charge of {responsibilityEntity.Entity} in {location}.");
                        },
                        async employee => // only one person found
                        {
                            await context.PostAsync($"'{employee.ToString()}' is the one to talk to if it comes to {responsibilityEntity.Entity} in {location}.");
                        },
                        async employeesList => // several employees found
                        {
                            var listOfPersons = String.Join("\n\n", employeesList.Select(e => e.ToString()));
                            await context.PostAsync($"Look at that! So many people can help you with {responsibilityEntity.Entity} in {location}:\n\n{listOfPersons}");
                        }
                    );
                }
            }
            else
            {
                await None(context, message, luisResult);
            }
        }

        [LuisIntent("PeopleAndRoles.WhatIsRoleOf")]
        public async Task WhatIsRoleOf(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult result)
        {
            var user = await context.GetCurrentUser(employeeService);
            EntityRecommendation personEntity;
            if (result.TryFindEntity(Constants.Entity_Person, out personEntity))
            {
                var locationEntity = GetDefaultLocationEntity(result, user);
                var person = await employeeService.GetPersonByLocationAndPersonName(locationEntity.Entity, personEntity.Entity);
                if (person == null)
                {
                    await context.PostAsync($"Can't find {personEntity.Entity}.");
                }
                else
                {
                    await context.PostAsync($"{person.ToString()} is {person.Role}");
                }
            }
            else
            {
                await None(context, message, result);
            }
        }

        /// NOT IMPLEMENTED
        /// <summary>
        /// To remove
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        //[LuisIntent("PeopleAndRoles.GetAllWithRole")]
        //public async Task GetAllWithRole(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult result)
        //{
        //    var user = await context.GetCurrentUser(employeeService);
        //    EntityRecommendation roleEntity;
        //    if (result.TryFindEntity(Constants.Entity_Role, out roleEntity))
        //    {
        //        var locationEntity = GetDefaultLocationEntity(result, user);
        //        var persons = await employeeService.GetPersonsByLocationAndRole(locationEntity.Entity, roleEntity.Entity);
        //        if (persons.Any())
        //        {
        //            await context.PostAsync("List are:");
        //            foreach (var person in persons) await context.PostAsync($"{person.ToString()}");
        //        }
        //        else
        //        {
        //            await context.PostAsync($"There is no employee with this role");
        //        }
        //    }
        //    else
        //    {
        //        await None(context, message, result);
        //    }
        //}

        /// <summary>
        /// To remove
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        //[LuisIntent("PeopleAndRoles.WhatAreResponsibilitiesForRole")]
        //public async Task WhatAreResponsibilitiesForRole(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult result)
        //{
        //    EntityRecommendation roleEntity;
        //    if (result.TryFindEntity(Constants.Entity_Role, out roleEntity))
        //    {
        //        var responsibilities = await employeeService.GetResponsibilitiesByRole(roleEntity.Entity);
        //        if (responsibilities.Any())
        //        {
        //            await context.PostAsync("List are:");
        //            foreach (var responsibility in responsibilities) await context.PostAsync($"{responsibility}");
        //        }
        //        else
        //        {
        //            await context.PostAsync($"It looks like guys with this role is lucky as they have no responsibilities.");
        //        }
        //    }
        //    else
        //    {
        //        await None(context, message, result);
        //    }
        //}

        /// <summary>
        /// To remove
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        //[LuisIntent("PeopleAndRoles.WhatAreResponsibilitiesOf")]
        //public async Task WhatAreResponsibilitiesOf(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult result)
        //{
        //    EntityRecommendation personEntity;
        //    if (result.TryFindEntity(Constants.Entity_Person, out personEntity))
        //    {
        //        var responsibilities = await employeeService.GetResponsibilitiesByPersonName(personEntity.Entity);
        //        if (responsibilities != null)
        //        {
        //            await context.PostAsync($"It is {responsibilities}");
        //        }
        //        else
        //        {
        //            await context.PostAsync($"It looks like this guy is lucky as there is no responsibilities assigned to him.");
        //        }
        //    }
        //    else
        //    {
        //        await None(context, message, result);
        //    }
        //}

        #endregion

        #region Dialog 3: Querying processes

        [LuisIntent("QueryProcess.DisplayProcess")]
        public async Task DisplayProcess(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult result)
        {
            var user = await context.GetCurrentUser(employeeService);
            EntityRecommendation processEntity;
            if (result.TryFindEntity(Constants.Entity_Process, out processEntity))
            {
                var locationEntity = GetDefaultLocationEntity(result, user);
                var process = await processService.GetProcess(processEntity.Entity, locationEntity.Entity);
                if (process != null)
                {
                    await context.PostAsync("I've found exactly the process you were looking for...");
                    await context.PostAsync(process.ToString());
                }
                else
                {
                    var processes = (await processService.GetProcessesLike(processEntity.Entity, locationEntity.Entity)).ToList();
                    if (processes.Any())
                    {
                        if (processes.Count == 1)
                        {
                            await context.PostAsync("This process looks like something you were looking for...");
                            await context.PostAsync(processes[0].ToString());
                        }
                        else
                        {
                            await context.PostAsync("Perhaps you were looking for one of the following...");
                            await context.PostAsync(string.Join(Environment.NewLine, processes.Select(p => p.ToShortString())));
                        }
                    }
                    else
                    {
                        await context.PostAsync("There are no processes with this name or looking alike.");
                    }
                }
            }
            else
            {
                await None(context, message, result);
            }
        }

        [LuisIntent("QueryProcess.WhoIsResponsibleForProcess")]
        public async Task WhoIsResponsibleForProcess(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult result)
        {
            var user = await context.GetCurrentUser(employeeService);
            EntityRecommendation processEntity;
            if (result.TryFindEntity(Constants.Entity_Process, out processEntity))
            {
                var locationEntity = GetDefaultLocationEntity(result, user);
                var process = await processService.GetProcess(processEntity.Entity, locationEntity.Entity);
                if (process != null)
                {
                    var person = string.IsNullOrEmpty(process.Responsible) ? null : await employeeService.TryGetUniqueEmployee(process.Responsible);
                    if (person != null)
                        await context.PostAsync($"{person} is the one responsible for {process.Name} in {process.Location}");
                    else
                        await context.PostAsync($"It seems that no one is responsible for {process.Name} in {process.Location}");
                }
                else
                {
                    await context.PostAsync($"Not able to find a process named {processEntity.Entity} in {locationEntity.Entity}");
                }                
            }
            else
            {
                await None(context, message, result);
            }
        }

        [LuisIntent("QueryProcess.GetListOfProcesses")]
        public async Task GetListOfProcesses(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult result)
        {
            var user = await context.GetCurrentUser(employeeService);
            var locationEntity = GetDefaultLocationEntity(result, user);
            var processes = (await processService.GetProcessesByLocation(locationEntity.Entity)).ToList();
            if (processes.Any())
            {
                await context.PostAsync($"There are the following processes in {processes[0].Location}:");
                foreach (var process in processes)
                    await context.PostAsync($"{process.ToShortString()}");
            }
            else
            {
                processes = (await processService.GetProcessesByLocationLike(locationEntity.Entity)).ToList();
                if (processes.Any())
                {                    
                    await context.PostAsync($"I hope you were looking for the processes in {processes[0].Location}. Here they are:");
                    foreach (var process in processes)
                        await context.PostAsync($"{process.ToShortString()}");
                }
                else
                {
                    await context.PostAsync($"Found no processes associated with {locationEntity.Entity}");
                }                
            }
        }
        #endregion

        #region Dialog 4: Querying the user context

        /// <summary>
        /// To remove
        /// </summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        //[LuisIntent("QueryUserContext.DisplayQueriesFor")]
        //public async Task DisplayQueriesFor(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult result)
        //{
        //    var user = await context.GetCurrentUser(employeeService);
        //    Range<DateTime>? period = GetPeriod(result);

        //    if (period.HasValue)
        //    {
        //        var queries = await queryService.GetMany(x => x.AskedBy == user.FullName && x.AskedOn > period.Value.Start && x.AskedOn < period.Value.After);
        //        if (queries.Any())
        //        {
        //            await context.PostAsync("List are:");
        //            foreach (var query in queries) await context.PostAsync($"{query}");
        //        }
        //        else
        //        {
        //            await context.PostAsync($"There are no any query.");
        //        }
        //    }
        //    else
        //    {
        //        await None(context, message, result);
        //    }
        //}

        [LuisIntent("QueryUserContext.DisplayAllUnansweredQueries")]
        public async Task DisplayAllUnansweredQueries(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult result)
        {
            var user = await context.GetCurrentUser(employeeService);
            Range<DateTime>? period = GetPeriod(result);

            var queries = await queryService.GetMany(x => x.AskedBy == user.FullName);

            if (period.HasValue)
            {
                queries = queries.Where(x =>
                    x.AskedOn < period.Value.After &&
                    x.AskedOn > period.Value.Start);
            }

            if (queries.Any())
            {
                await context.PostAsync("Here are our unanswered questions:");
                foreach (var query in queries) await context.PostAsync($"{query}");
            }
            else
            {
                await context.PostAsync($"There are no unanswered questions at the moment.");
            }
        }

        #endregion
    }
}