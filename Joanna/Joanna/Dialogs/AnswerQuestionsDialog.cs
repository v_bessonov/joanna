﻿using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Internals.Fibers;
using Joanna.Services;
using Microsoft.Bot.Connector;
using Joanna.Models;
using Microsoft.Bot.Builder.FormFlow;
using Joanna.Helpers;
using System.Configuration;

namespace Joanna.Dialogs
{
    public interface IAnswerQuestionsDialog : IDialog<object>
    {

    }

    [Serializable]
    public class AnswerQuestionsDialog : IAnswerQuestionsDialog
    {
        private readonly IEmployeeService _employeeService;
        private readonly IQuestionDbService _questionDbService;

        public AnswerQuestionsDialog(IEmployeeService employeeService, IQuestionDbService questionDbService)
        {
            SetField.NotNull(out _employeeService, nameof(employeeService), employeeService);
            SetField.NotNull(out _questionDbService, nameof(questionDbService), questionDbService);
        }

        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
        }

        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> item)
        {
            var message = await item;

            await AskQuestion(context);
        }

        private const string Delete = "Delete";
        private const string Answer = "Answer";
        private const string Skip = "Skip";
        private const string Exit = "Exit";

        [Serializable]
        private class AnswerOption
        {
            public string QuestionId;
            public string SelectedOption;

            public override string ToString()
            {
                return SelectedOption;
            }
        }

        private async Task AskQuestion(IDialogContext context)
        {
            var triedToAnswer = context.PrivateConversationData.ContainsKey(Constants.TriedToAnswerQuestionsKey) ?
                            context.PrivateConversationData.Get<List<string>>(Constants.TriedToAnswerQuestionsKey) : new List<string>();
            var question = (await _questionDbService.GetAllUnanswered()).Where(x => !triedToAnswer.Contains(x.id)).Reverse().FirstOrDefault();
            if (question == null)
            {
                if (triedToAnswer.Any())
                    context.PrivateConversationData.SetValue(Constants.TriedToAnswerQuestionsKey, new List<string>());
                await context.PostAsync("Oh, it looks like there's no unanswered question left. Great job and thank you!");
                await QnAServiceExtensions.PublishQnAService(
                            ConfigurationManager.AppSettings["QnASubscriptionKey"],
                            ConfigurationManager.AppSettings["QnAKnowledgebaseId"]
                            );
                context.Done(string.Empty);
            }
            else
            {
                PromptDialog.Choice(context, this.OnOptionSelected, new List<AnswerOption>() {
                    new AnswerOption { SelectedOption = Delete, QuestionId = question.id },
                    new AnswerOption { SelectedOption = Answer, QuestionId = question.id },
                    new AnswerOption { SelectedOption = Skip, QuestionId = question.id },
                    new AnswerOption { SelectedOption = Exit, QuestionId = question.id }
                }, $"{question.Text}", "Not a valid option", 3);
            }
        }

        private async Task OnOptionSelected(IDialogContext context, IAwaitable<AnswerOption> result)
        {
            try
            {
                var option = await result;

                switch (option.SelectedOption)
                {
                    case Exit:
                        await QnAServiceExtensions.PublishQnAService(
                            ConfigurationManager.AppSettings["QnASubscriptionKey"],
                            ConfigurationManager.AppSettings["QnAKnowledgebaseId"]
                            );
                        await context.PostAsync("Thank you for your assistance!");
                        context.Done(string.Empty);
                        break;

                    case Skip:
                        var triedToAnswer = context.PrivateConversationData.ContainsKey(Constants.TriedToAnswerQuestionsKey) ?
                        context.PrivateConversationData.Get<List<string>>(Constants.TriedToAnswerQuestionsKey) : new List<string>();
                        triedToAnswer.Add(option.QuestionId);
                        context.PrivateConversationData.SetValue(Constants.TriedToAnswerQuestionsKey, triedToAnswer);
                        await AskQuestion(context);
                        break;

                    case Delete:
                        await _questionDbService.Delete(option.QuestionId);
                        await AskQuestion(context);
                        break;

                    case Answer:
                        BuildFormDelegate<AnswerQuestion> answerQuestion = AnswerQuestion.BuildForm;
                        var answerQuestionForm = new FormDialog<AnswerQuestion>(new AnswerQuestion(option.QuestionId), answerQuestion, FormOptions.PromptInStart);
                        context.Call(answerQuestionForm, ResumeAfterAnsweredQuestion);
                        break;
                }
            }
            catch (TooManyAttemptsException)
            {
                await context.PostAsync($"Ooops! Too many attemps :(. But don't worry, I'm handling that exception and you can try again!");

                await AskQuestion(context);
            }
        }

        private async Task ResumeAfterAnsweredQuestion(IDialogContext context, IAwaitable<AnswerQuestion> result)
        {
            AnswerQuestion questionAnswer;
            try
            {
                questionAnswer = await result;
            }
            catch (OperationCanceledException)
            {
                await context.PostAsync("Unfortunately I need to know your answer!");
                return;
            }
            
            var question = await _questionDbService.GetQuestionById(questionAnswer.Id);
            var answerBy = (await context.GetCurrentUser(_employeeService)).FullName;

            try
            {
                await _questionDbService.AnswerQuestion(question, questionAnswer.Answer, answerBy);

                await QnAServiceExtensions.UpdateQnAService(
                        ConfigurationManager.AppSettings["QnASubscriptionKey"],
                        ConfigurationManager.AppSettings["QnAKnowledgebaseId"],
                        question.Text,
                        questionAnswer.Answer
                        );
            }
            catch (Exception ex)
            {
                await context.PostAsync($":( {ex.Message}");
            }
            await AskQuestion(context);
        }
    }
}