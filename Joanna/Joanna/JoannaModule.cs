﻿using Autofac;
using Joanna.Dialogs;
using Joanna.Services;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Internals.Fibers;
using Microsoft.Bot.Builder.Luis;
using QnAMakerDialog;
using System;
using System.Configuration;
namespace Joanna
{
    public sealed class JoannaModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(c => new LuisModelAttribute(ConfigurationManager.AppSettings["LuisModelId"], ConfigurationManager.AppSettings["LuisSubscriptionId"])).AsSelf().AsImplementedInterfaces().SingleInstance();

            // register the top level dialog
            builder.RegisterType<AnswerQuestionsDialog>().As<IAnswerQuestionsDialog>().InstancePerDependency();
            builder.RegisterType<UserAuthenticationDialog>().As<IDialog<object>>().InstancePerDependency();
            builder.RegisterType<LuisRootDialog>().As<LuisDialog<object>>().InstancePerDependency();
            builder.RegisterType<QnADialog>().As<QnAMakerDialog<Guid>>().InstancePerDependency();

            // register some singleton services
            builder.RegisterType<LuisService>().Keyed<ILuisService>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ResolutionParser>().Keyed<IResolutionParser>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WesternCalendarPlus>().Keyed<ICalendarPlus>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<StrictEntityToType>().Keyed<IEntityToType>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().SingleInstance();

            builder.RegisterType<EmployeeService>().Keyed<IEmployeeService>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ProcessService>().Keyed<IProcessService>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<QuestionDbService>().Keyed<IQuestionDbService>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ShortAnswerService>().Keyed<IShortAnswerService>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().SingleInstance();
        }
    }
}