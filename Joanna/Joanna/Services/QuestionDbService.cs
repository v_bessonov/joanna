﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Joanna.Models;
using Joanna.Repositories;

namespace Joanna.Services
{
    public interface IQuestionDbService
    {
        Task<Question> CreateAndSaveNewQuestion(string questionAsked, string user);
        Task<IEnumerable<Question>> GetAllUnanswered();
        Task<Question> AnswerQuestion(Question question, string answer, string answeredBy);
        Task<IEnumerable<Question>> GetMany(Expression<Func<Question, bool>> filter);
        Task<Question> GetQuestionById(string guid);
        Task Delete(string questionId);
    }

    public class QuestionDbService : IQuestionDbService
    {
        public async Task<Question> CreateAndSaveNewQuestion(string questionAsked, string user)
        {
            var newQuestion = new Question()
            {
                id = Guid.NewGuid().ToString(),
                Text = questionAsked,
                AskedOn = DateTime.UtcNow,
                AskedBy = user,
                WasAnswered = false,
                Answer = String.Empty,
                AnsweredBy = String.Empty,
                AnsweredOn = null
            };

            var document = await DocumentDbRepository<Question>.CreateAsync(DbCollections.Questions, newQuestion);
            Question question = (dynamic) document;

            return question;
        }

        public async Task<IEnumerable<Question>> GetAllUnanswered()
        {
            var unansweredQuestions = await GetMany(q => q.WasAnswered == false);
            return unansweredQuestions;
        }

        public async Task<Question> GetQuestionById(string guid)
        {
            var questions = await GetMany(q => q.id == guid);
            return questions.FirstOrDefault();
        }

        /// <summary>
        /// Answer a question and saves it to the DB.
        /// WARNING! The question must exist already and have a valid ID!
        /// </summary>
        public async Task<Question> AnswerQuestion(Question questionToBeAnswered, string answer, string answeredBy)
        {
            if (String.IsNullOrEmpty(questionToBeAnswered.id))
                throw new InvalidOperationException("the question must exist in the DB first!");

            questionToBeAnswered.WasAnswered = true;
            questionToBeAnswered.Answer = answer;
            questionToBeAnswered.AnsweredBy = answeredBy;
            questionToBeAnswered.AnsweredOn = DateTime.UtcNow;
            
            var document = await DocumentDbRepository<Question>.UpdateAsync(DbCollections.Questions, questionToBeAnswered.id, questionToBeAnswered);
            Question question = (dynamic) document;

            return question;
        }

/// <summary>
        /// Answer any advanced query on questions.
        /// Make sure your query only contains simple instructions, otherwise they won't be convertable to the underlying querying language.
        /// Make sure to use 'ToLower()' to make case-insensitve comparisons if needed.
        /// </summary>
        public async Task<IEnumerable<Question>> GetMany(Expression<Func<Question, bool>> filter)
        {
            var questions = await DocumentDbRepository<Question>.GetManyAsync(DbCollections.Questions, filter);
            return questions;
        }

        public async Task Delete(string questionId)
        {
            await DocumentDbRepository<Question>.DeleteAsync(questionId, DbCollections.Questions);
        }
    }
}