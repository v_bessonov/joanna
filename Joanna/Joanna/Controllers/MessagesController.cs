﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Autofac;
using Microsoft.Bot.Builder.Internals.Fibers;
using System.Threading;

namespace Joanna.Controllers
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        private readonly ILifetimeScope scope;
        public MessagesController(ILifetimeScope scope )
        {
            SetField.NotNull(out this.scope, nameof(scope), scope);
        }
        [BotAuthentication]
        public virtual async Task<HttpResponseMessage> Post([FromBody]Activity activity, CancellationToken token)
        {
            if (activity != null && activity.GetActivityType() == ActivityTypes.Message)
            {
                if (activity.Conversation.IsGroup.HasValue && activity.Conversation.IsGroup.Value) return new HttpResponseMessage(System.Net.HttpStatusCode.Accepted);
                using (var scope = DialogModule.BeginLifetimeScope(this.scope, activity))
                {
                    // HACK: Please do not remove the following lines.
                    // They're needed to overcome the issue in the Converasation class (https://github.com/Microsoft/BotBuilder/issues/1956).
                    var dialog = scope.Resolve<IDialog<object>>();
                    DialogModule_MakeRoot.Register(scope, () => dialog);

                    var postToBot = scope.Resolve<IPostToBot>();
                    await postToBot.PostAsync(activity, token);
                }
            }
            else
            {
                HandleSystemMessage(activity);
            }

            return new HttpResponseMessage(System.Net.HttpStatusCode.Accepted);
        }

        private Activity HandleSystemMessage(Activity message)
        {
            if (message.Type == ActivityTypes.Ping)
            {
                Activity reply = message.CreateReply();
                reply.Type = ActivityTypes.Ping;
                return reply;
            }
            else if (message.Type == ActivityTypes.DeleteUserData)
            {
                var stateClient = message.GetStateClient();
                stateClient.BotState.DeleteStateForUser(message.ChannelId, message.From.Id);
                Activity reply = message.CreateReply();
                reply.Type = ActivityTypes.DeleteUserData;
                return reply;
            }

            return null;
        }
    }
}