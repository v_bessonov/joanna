﻿using Joanna.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;

namespace Joanna.Helpers
{
    public static class LuisExtensions
    {
        public static string TryGetLocation(this LuisResult luisResult)
        {
            EntityRecommendation location;
            var success = luisResult.TryFindEntity(Constants.Entity_Location, out location);
            return success ? location.Entity : null;
        }
    }
}