﻿using Joanna.Dialogs;
using Joanna.Models;
using Joanna.Services;
using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Threading.Tasks;

namespace Joanna.Helpers
{
    public static class DialogContextExtensions
    {
        public static async Task<Employee> GetCurrentUser(this IDialogContext context, IEmployeeService service)
        {
            var currentUserGuid = context.UserData.Get<Guid>(Constants.PersonKey);
            return await service.TryGetUniqueEmployeeById(currentUserGuid);
        }
    }
}