﻿Where is the IT department located in Canada?
 The IT wing is located in the South-East corner on the 10th floor of our Infusion Toronto office. If you’re having trouble finding us you can always ask at reception – we’re right after the Kitchen!

What is the difference between Infusion IT and Infusion IT Support?
 - Infusion IT is responsible for Infusion’s IT Infrastructure and IT Support Services
 - IT Support is just one facet of what IT services provided by Infusion
 - IT Support covers user requests, general day to day support tasks, as well as IT inventory and asset control

How do I request support, equipment or anything else IT related?
- Need help with something? Whether your laptop has suddenly crashed or you need to be provided with access in a TFS Project, you can always reach out to IT Support for some help. We handle all types of requests such as support and troubleshooting, equipment provisioning and upgrades, project and site creation and permissions - even building access keycards.The best way to reach us is to email itsupport@somecompanyname.com
- Note that any equipment request must be placed in the procurement portal