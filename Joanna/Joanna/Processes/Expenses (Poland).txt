﻿When creating a travel expense make sure you:
	- have chosen a correct date from the data field
	- have chosen a right project code - confirm that with your Project Manager / Manager
	- have chosen a correct expense item from the drop down list
	- have chosen a right currency;
	- logged billable and non-billable items correctly
	- you have provided a travel data and information about meals
	- if meals were provided please remember about per diems distractions
	- attach all the support documentation (receipts, bills, invices etc.) to the expense

Before a team outing event:
	- please note Team Outing can be expensed only by a Team Manager
	- make sure you have chosen a correct Project Code of Department Social Budget Expenses
	- make sure your team outing event (including all types of meals) and its budget is approved
	- attach the approval in the expense
	- list the names of attendees in the notes
	- provide an invoice (preferably)

As a peer mentor you can take out for a lunch your mentee and expense it.
	- Peer mentor outing should be 75 PLN per couple (two persons) and logged under the correct code in Open Air
	- Please remember to write the name of the person that you went to lunch with in the expense report
	- Please reach out to HR to check the Peer Mentor program in details

Relocation expenses:
 - Need to be discussed with HR / TAS team
 - Should be within a relocation allowance limit
 - All expensed items should be compliant with an Offer Letter or pre-approved by HR before expensing
 - When booking a flight outside of Egencia, please consult HR and Travel Coordinator
 - Should be booked under Project Code Relocation and Visas, in case you don't have the code please reach out to Project Coordinators
 - Should be logged as non-billable
 - In case you have any doubts or questions, please refer to you offer letter for conditions and more information. 