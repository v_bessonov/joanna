﻿using System.Configuration;
using System.Web;
using System.Web.Http;
using Joanna.Models;
using Joanna.Repositories;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Autofac;
using Autofac.Integration.WebApi;
using System.Reflection;
using Microsoft.Bot.Builder.Dialogs;
using Elmah.Contrib.WebApi;
using System.Web.Http.ExceptionHandling;

namespace Joanna
{
    public class WebApiApplication : HttpApplication
    {
        private void SetAuthenticationSettings()
        {
            AuthBot.Models.AuthSettings.Mode = ConfigurationManager.AppSettings["ActiveDirectory.Mode"];
            AuthBot.Models.AuthSettings.EndpointUrl = ConfigurationManager.AppSettings["ActiveDirectory.EndpointUrl"];
            AuthBot.Models.AuthSettings.Tenant = ConfigurationManager.AppSettings["ActiveDirectory.Tenant"];
            AuthBot.Models.AuthSettings.RedirectUrl = ConfigurationManager.AppSettings["ActiveDirectory.RedirectUrl"];
            AuthBot.Models.AuthSettings.ClientId = ConfigurationManager.AppSettings["ActiveDirectory.ClientId"];
            AuthBot.Models.AuthSettings.ClientSecret = ConfigurationManager.AppSettings["ActiveDirectory.ClientSecret"];
        }

        protected void Application_Start()
        {
            SetAuthenticationSettings();

            var builder = new ContainerBuilder();

            builder.RegisterModule(new DialogModule());
            builder.RegisterModule(new JoannaModule());

            var config = GlobalConfiguration.Configuration;

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterWebApiFilterProvider(config);

            // HACK: Please do not remove the following lines.
            // They're needed to overcome the issue in the Converasation class (https://github.com/Microsoft/BotBuilder/issues/1956).
            builder.Update(Conversation.Container);
            var container = Conversation.Container;

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            GlobalConfiguration.Configure(WebApiConfig.Register);

            // Initialize client for DocumentDB
            DocumentDbRepository<Employee>.Initialize();
            DocumentDbRepository<Question>.Initialize();
            DocumentDbRepository<Process>.Initialize();

            config.Services.Add(typeof(IExceptionLogger), new ElmahExceptionLogger());
        }
    }
}