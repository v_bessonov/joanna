﻿using FluentAssertions;
using Joanna.Models;
using Joanna.Repositories;
using Joanna.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Joanna.Tests
{
    [TestFixture, Category("Integration")]
    public class ProcessServiceTests
    {
        [Test]
        public void GetOne()
        {
            // Arrange
            DocumentDbRepository<Process>.Initialize();
            var sut = new ProcessService();
            var procName = "Relocation";
            var procLocation = "Toronto";

            // Act
            var result = sut.GetProcess(procName, procLocation).Result;

            // Assert
            result.Should().NotBeNull();
            result.Name.Should().Be(procName);
            result.Location.Should().Be(procLocation);
            result.Description.Should().NotBeNullOrWhiteSpace();
        }

        [Test]
        public void GetOneLowercase()
        {
            // Arrange
            DocumentDbRepository<Process>.Initialize();
            var sut = new ProcessService();
            var procName = "relocations";
            var procLocation = "toronto";

            // Act
            var result = sut.GetProcess(procName, procLocation).Result;

            // Assert
            result.Should().NotBeNull();
            result.Name.ToLower().Should().Be(procName);
            result.Location.ToLower().Should().Be(procLocation);
            result.Description.Should().NotBeNullOrWhiteSpace();
        }

        [Test]
        public void GetProcessesLike()
        {
            // Arrange
            DocumentDbRepository<Process>.Initialize();
            var sut = new ProcessService();
            var procName = "location";
            var procLocation = "toronto";

            // Act
            var result = sut.GetProcessesLike(procName, procLocation).Result;

            // Assert
            result.Should().NotBeNull();
            result.Count().Should().BeGreaterThan(0);
            Assert.IsTrue(result.Any(p => p.Name == "Relocation"));
            Assert.IsTrue(result.Any(p => p.Location == "Toronto"));            
        }

        [Test]
        public void GetMany()
        {
            // Arrange
            DocumentDbRepository<Process>.Initialize();
            var sut = new ProcessService();
            var procName = "Relocation";
            var procLocation1 = "Toronto";
            var procLocation2 = "New York";

            // Act
            var result = sut.GetMany(proc => proc.Name == procName).Result;

            // Assert
            result.Should().NotBeNullOrEmpty();
            result.Should().ContainSingle(p => p.Location == procLocation1);
            result.Should().ContainSingle(p => p.Location == procLocation2);
        }

        [Test]
        public void GetManyWithFilterByNameContains()
        {
            // Arrange
            DocumentDbRepository<Process>.Initialize();
            var sut = new ProcessService();
            var procNamePart = "loca";
            var procLocation1 = "Toronto";
            var procLocation2 = "New York";

            // Act
            var result = sut.GetMany(proc => proc.Location == procLocation1 && proc.Name.Contains(procNamePart)).Result;

            // Assert
            result.Should().NotBeNullOrEmpty();
            result.Should().Contain(p => p.Location == procLocation1);
            result.Should().NotContain(p => p.Location == procLocation2);
        }
        
        [Test]
        public void GetProcessesByLocation()
        {
            // Arrange
            DocumentDbRepository<Process>.Initialize();
            var sut = new ProcessService();            
            var procLocation1 = "Toronto";
            var procLocation2 = "New York";

            // Act
            var result = sut.GetProcessesByLocation(procLocation1).Result;

            // Assert
            result.Should().NotBeNullOrEmpty();
            result.Should().Contain(p => p.Location == "Toronto");
            result.Should().NotContain(p => p.Location == procLocation2);
        }

        [Test]
        public void GetProcessesByLocationLike()
        {
            // Arrange
            DocumentDbRepository<Process>.Initialize();
            var sut = new ProcessService();
            var procLocation1 = "ro";
            var procLocation2 = "New York";

            // Act
            var result = sut.GetProcessesByLocationLike(procLocation1).Result;

            // Assert
            result.Should().NotBeNullOrEmpty();
            result.Should().Contain(p => p.Location == "Toronto");
            result.Should().Contain(p => p.Location == "Wrocław");
            result.Should().NotContain(p => p.Location == procLocation2);
        }

        private static readonly Dictionary<string, string[]> _locationsMap = new Dictionary<string, string[]>()
        {
            {" (Global)", new [] { "Wrocław", "Kraków", "Toronto", "New York", "Raleigh", "London" } },
            {" (Poland)", new [] { "Wrocław", "Kraków" } },
            {" (Canada)", new [] { "Toronto" } },
            {" (USA)", new [] { "New York", "Raleigh" } },
            {" (UK)", new [] { "London" } },
        };

        // [Test] // Uncomment to run! Beware, it will upload data to the database!
        public void UploadProcesses()
        {
            const string processFolderPath = @"D:\Repos\Joanna\Joanna\Joanna\Processes";
            var processes = GetProcessesFromTextFiles(processFolderPath);
            
            Console.WriteLine("Saving the following processes to Azure DB: " + string.Join(" ", processes.Select(p => p.Name)));
            DocumentDbRepository<Process>.Initialize();

            foreach (var process in processes)            
                foreach (var fileNamePart in _locationsMap.Keys)                
                    if (process.Name.Contains(fileNamePart))
                    {
                        process.Name = process.Name.Replace(fileNamePart, string.Empty);
                        CreateProcessForLocations(process, _locationsMap[fileNamePart]);                        
                    }
        }

        private void CreateProcessForLocations(Process process, params string[] locations)
        {
            foreach (var location in locations)
            {
                process.Location = location;
                DocumentDbRepository<Process>.CreateAsync(DbCollections.Processes, process).Wait();
            }
        }

        private List<Process> GetProcessesFromTextFiles(string folderPath)
        {
            var dir = new DirectoryInfo(folderPath);
            var files = dir.GetFiles("*.txt");

            return files.Select(file => 
                new Process
                {
                    Name = Path.GetFileNameWithoutExtension(file.FullName),
                    Description = File.ReadAllText(file.FullName)
                }).ToList();            
        }
    }
}
